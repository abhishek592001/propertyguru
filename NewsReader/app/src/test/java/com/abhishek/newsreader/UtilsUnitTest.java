package com.abhishek.newsreader;
import com.abhishek.newsreader.model.StoryItem;
import com.abhishek.newsreader.utils.TopStoriesTask;
import com.abhishek.newsreader.utils.Utility;
import com.abhishek.newsreader.viewmodel.HomeViewModel;

import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Abhishek on 21-06-2017.
 */

public class UtilsUnitTest {

    @Test
    public void verifyGetResponseWithInvalidUrl(){
        String response = Utility.getResponse(null);
        assertEquals(response, "");
    }

    @Test
    public void verifyGetResponseWithValidUrl(){
        String response = Utility.getResponse(Utility.TOP_STORIES_URL);
        assertNotEquals(response, "");
    }

    @Test
    public void verifyLoadingText(){
        assertEquals(Utility.LOADING_TEXT, "Loading...");
    }

    @Test
    public void verifyTopStoriesUrl(){
        assertEquals(Utility.TOP_STORIES_URL, "https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty");
    }

    @Test
    public void verifyItemUrl(){
        long itemId = 1394459;
        assertEquals(Utility.ITEM_URL_PREFIX + itemId + Utility.ITEM_URL_SUFFIX, "https://hacker-news.firebaseio.com/v0/item/" + itemId +".json?print=pretty");
    }

}
