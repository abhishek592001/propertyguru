package com.abhishek.newsreader;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.abhishek.newsreader.model.CommentItem;
import com.abhishek.newsreader.model.StoryItem;
import com.abhishek.newsreader.view.CommentsAdapter;
import com.abhishek.newsreader.view.HomeActivity;
import com.abhishek.newsreader.view.TopStoriesAdapter;

import org.junit.Test;
import org.mockito.Mockito;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Abhishek on 21-06-2017.
 */
public class ViewUniteTest {

    @Test
    public void verifyTopStoriesAdapterInitialization(){

        HomeActivity activity = Mockito.mock(HomeActivity.class);
        TopStoriesAdapter adapter = Mockito.spy(new TopStoriesAdapter(activity));

        List<StoryItem> stories = new ArrayList<>();
        stories.add(new StoryItem(122133));
        stories.add(new StoryItem(1256133));

        try {
            //fixAdapterForTesting(adapter);
            adapter.updateStories(stories);
        }
        catch(Exception e){

        }

        assertEquals(adapter.getItemCount(), stories.size());
    }

    @Test
    public void verifyCommentsAdapterInitialization() throws NoSuchFieldException, IllegalAccessException {

        List<CommentItem> comments = new ArrayList<>();
        comments.add(new CommentItem(16347434));
        comments.add(new CommentItem(34555455));
        CommentsAdapter adapter = new CommentsAdapter(comments);

        assertEquals(adapter.getItemCount(), comments.size());
    }

    @Test
    public void verifyHomeActivity(){
        RecyclerView recyclerView = Mockito.mock(RecyclerView.class);
        RecyclerView.LayoutManager layoutManager = Mockito.mock(RecyclerView.LayoutManager.class);
        when(recyclerView.getLayoutManager()).thenReturn(layoutManager);
        assertEquals(recyclerView.getLayoutManager(), layoutManager);
    }

    public static RecyclerView.AdapterDataObserver fixAdapterForTesting(RecyclerView.Adapter adapter) throws NoSuchFieldException, IllegalAccessException {
        // Observables are not mocked by default so we need to hook the adapter up to an observer so we can track changes
        Field observableField = RecyclerView.Adapter.class.getDeclaredField("mObservable");
        observableField.setAccessible(true);
        Object observable = observableField.get(adapter);
        Field observersField = Observable.class.getDeclaredField("mObservers");
        observersField.setAccessible(true);
        final ArrayList<Object> observers = new ArrayList<>();
        RecyclerView.AdapterDataObserver dataObserver = Mockito.mock(RecyclerView.AdapterDataObserver.class);
        observers.add(dataObserver);
        observersField.set(observable, observers);
        return dataObserver;
    }
}
