package com.abhishek.newsreader;

import com.abhishek.newsreader.model.CommentItem;
import com.abhishek.newsreader.model.StoryItem;
import com.abhishek.newsreader.utils.StoryItemTask;
import com.abhishek.newsreader.utils.TopStoriesTask;
import com.abhishek.newsreader.utils.Utility;
import com.abhishek.newsreader.view.HomeActivity;
import com.abhishek.newsreader.viewmodel.CommentItemViewModel;
import com.abhishek.newsreader.viewmodel.HomeViewModel;
import com.abhishek.newsreader.viewmodel.StoryItemViewModel;

import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.mockito.internal.matchers.Any;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Abhishek on 21-06-2017.
 */

public class ViewModelUnitTest {

    @Test
    public void verifyStoryItemViewModel(){

        StoryItem storyItem = new StoryItem(433443);
        storyItem.Title = "Android is on boom";
        storyItem.By = "abhishek";
        StoryItemViewModel storyItemViewModel = Mockito.mock(StoryItemViewModel.class);
        when(storyItemViewModel.getStory()).thenReturn(storyItem);
        when(storyItemViewModel.getTitle()).thenReturn(storyItem.Title);

        assertEquals(433443, storyItemViewModel.getStory().Id);
        assertEquals("Android is on boom", storyItemViewModel.getTitle());
        assertEquals("abhishek", storyItemViewModel.getStory().By);

        storyItemViewModel.setStory(storyItem);
        verify(storyItemViewModel, atLeastOnce()).setStory(storyItem);
    }

    @Test
    public void verifyCommentItemViewModel(){

        CommentItem commentItem = new CommentItem(433443);
        commentItem.Comment = "Android is on boom";
        commentItem.By = "abhishek";
        commentItem.ChildId = 322332;
        commentItem.ChildBy ="vishal";
        commentItem.ChildComment = "Yes very correct but soon surface phone will come";
        CommentItemViewModel commentItemViewModel = Mockito.mock(CommentItemViewModel.class);

        when(commentItemViewModel.getCommentItem()).thenReturn(commentItem);
        when(commentItemViewModel.getCommentText()).thenReturn(commentItem.Comment);
        when(commentItemViewModel.getBy()).thenReturn(commentItem.By);
        when(commentItemViewModel.getChildComment()).thenReturn(commentItem.ChildComment);
        when(commentItemViewModel.getChildBy()).thenReturn(commentItem.ChildBy);

        assertEquals(433443, commentItemViewModel.getCommentItem().Id);
        assertEquals("Android is on boom", commentItemViewModel.getCommentText());
        assertEquals("abhishek", commentItemViewModel.getBy());
        assertEquals(322332, commentItemViewModel.getCommentItem().ChildId);
        assertEquals("Yes very correct but soon surface phone will come", commentItemViewModel.getChildComment());
        assertEquals("vishal", commentItemViewModel.getChildBy());

        commentItemViewModel.setComment(commentItem);
        verify(commentItemViewModel, atLeastOnce()).setComment(commentItem);
    }

    @Test
    public void verifyHomeViewModelUpdateMethod() {

        HomeViewModel homeViewModel = Mockito.spy(HomeViewModel.class);
        HomeActivity homeActivity = Mockito.spy(HomeActivity.class);
        homeViewModel.addObserver(homeActivity);
        List<StoryItem> stories = new ArrayList<>();
        stories.add(new StoryItem(122133));
        stories.add(new StoryItem(1256133));
        homeViewModel.updateStories(stories);

        verify(homeViewModel, atLeastOnce()).updateStories(stories);
        verify(homeViewModel, atLeastOnce()).notifyObservers(stories);
        verify(homeActivity, atLeastOnce()).update(any(HomeViewModel.class), any(Object.class));
    }
}
