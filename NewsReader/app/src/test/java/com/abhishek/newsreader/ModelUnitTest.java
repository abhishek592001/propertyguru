package com.abhishek.newsreader;

import com.abhishek.newsreader.model.CommentItem;
import com.abhishek.newsreader.model.StoryItem;
import com.abhishek.newsreader.utils.Utility;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Abhishek on 21-06-2017.
 */

public class ModelUnitTest {
    @Test
    public void verifyDefaultCommenttext(){
        CommentItem item = new CommentItem(1000);
        assertEquals(item.Comment, Utility.LOADING_TEXT);
    }

    @Test
    public void verifydefaultStoryTitle(){
        StoryItem item = new StoryItem(1000);
        assertEquals(item.Title, Utility.LOADING_TEXT);
    }
}
