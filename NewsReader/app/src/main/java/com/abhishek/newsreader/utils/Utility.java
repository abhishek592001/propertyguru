package com.abhishek.newsreader.utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Abhishek on 20-06-2017.
 */

public class Utility {

    public static final String TOP_STORIES_URL = "https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty";
    public static final String ITEM_URL_PREFIX = "https://hacker-news.firebaseio.com/v0/item/";
    public static final String ITEM_URL_SUFFIX = ".json?print=pretty";
    public static final String LOADING_TEXT = "Loading...";

    /**
     * makes a get call and return the response string
     * DO not call this method from main thread
     * @param url
     * @return
     */
    public static String getResponse(String url) {
        InputStream inputStream = null;
        String result = "";
        URLConnection urlConnection = null;

        try {
            URL newurl = new URL(url);

            urlConnection = newurl.openConnection();
            inputStream = urlConnection.getInputStream();

            // convert inputstream to string
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
        } catch (Exception e) {

            result = "";
        } finally {

        }

        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream)
            throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}
