package com.abhishek.newsreader.model;

import com.abhishek.newsreader.utils.Utility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhishek on 20-06-2017.
 */

public class StoryItem {

    public StoryItem(long id){
        Id = id;
        Comments = new ArrayList<>();
        Title = Utility.LOADING_TEXT;
    }

    public long Id;
    public String By;
    public List<Long> Comments;
    public String Title;
}
