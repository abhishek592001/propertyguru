package com.abhishek.newsreader.model;

import com.abhishek.newsreader.utils.Utility;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhishek on 20-06-2017.
 */

public class CommentItem {

    public CommentItem(long id){
        Id = id;
        Comment = Utility.LOADING_TEXT;
    }

    public long Id;
    public String By;
    public String Comment;
    public long ChildId;
    public String ChildBy;
    public String ChildComment;
}
