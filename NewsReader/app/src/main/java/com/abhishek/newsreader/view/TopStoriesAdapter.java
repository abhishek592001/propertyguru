package com.abhishek.newsreader.view;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.abhishek.newsreader.R;
import com.abhishek.newsreader.databinding.StoryItemRowBinding;
import com.abhishek.newsreader.model.StoryItem;
import com.abhishek.newsreader.viewmodel.StoryItemViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhishek on 20-06-2017.
 */

public class TopStoriesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<StoryItem> mStories;
    private Context mContext;

    public TopStoriesAdapter(Context context){
        mStories = new ArrayList<>();
        mContext = context;
    }

    public void updateStories(List<StoryItem> stories){
        mStories.addAll(stories);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        StoryItemRowBinding storyItemRowBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.story_item_row, parent, false);
        return new StoryItemViewHolder(storyItemRowBinding);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((StoryItemViewHolder)holder).bind(mStories.get(position));
    }

    @Override
    public int getItemCount() {
        return mStories.size();
    }

    public void goToCommentActivity(StoryItem story){
        Intent intent = new Intent(mContext, CommentsActivity.class);
        Bundle b = new Bundle();
        long[] array = new long[story.Comments.size()];
        for(int i = 0; i < array.length; i++){
            array[i] = story.Comments.get(i);
        }
        b.putLongArray("CommentsId", array);
        intent.putExtras(b);
        mContext.startActivity(intent);
    }

    public class StoryItemViewHolder extends RecyclerView.ViewHolder{

        private StoryItemRowBinding mBinding;

        public StoryItemViewHolder(StoryItemRowBinding storyItemRowBinding){
            super(storyItemRowBinding.storyItem);
            mBinding = storyItemRowBinding;

        }

        public void bind(final StoryItem story){

            if(mBinding.getViewModel() == null){
                mBinding.setViewModel(new StoryItemViewModel(story));
            }
            else{
                mBinding.getViewModel().setStory(story);
            }
            mBinding.storyItem.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    //listener.onItemClick(item);
               goToCommentActivity(story);
                }
            });
        }
    }

}
