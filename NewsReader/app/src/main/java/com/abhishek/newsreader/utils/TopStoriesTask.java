package com.abhishek.newsreader.utils;

import android.os.AsyncTask;

import com.abhishek.newsreader.model.StoryItem;
import com.abhishek.newsreader.view.TopStoriesAdapter;
import com.abhishek.newsreader.viewmodel.HomeViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhishek on 20-06-2017.
 */

public class TopStoriesTask extends AsyncTask<String,Void, List<StoryItem>> {

    private HomeViewModel mViewModel;

    //Unable to mock Async task thas's why test case is not written
    public TopStoriesTask(HomeViewModel homeViewModel){
        mViewModel = homeViewModel;
    }

    @Override
    protected List<StoryItem> doInBackground(String... params) {
        List<StoryItem> stories = new ArrayList<>();

        try {
            String response = Utility.getResponse(Utility.TOP_STORIES_URL);
            JSONArray array = new JSONArray(response);
            int l = array.length();
            for(int i = 0; i < l; i++){
                stories.add(new StoryItem(array.getLong(i)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return stories;
    }

    @Override
    protected void onPostExecute(List<StoryItem> stories) {
        super.onPostExecute(stories);
        mViewModel.updateStories(stories);
    }
}
