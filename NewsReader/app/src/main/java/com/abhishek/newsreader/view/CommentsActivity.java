package com.abhishek.newsreader.view;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.abhishek.newsreader.R;
import com.abhishek.newsreader.databinding.ActivityCommentsBinding;
import com.abhishek.newsreader.model.CommentItem;
import com.abhishek.newsreader.viewmodel.CommentActivityViewModel;

import java.util.ArrayList;
import java.util.List;

public class CommentsActivity extends AppCompatActivity {

    private ActivityCommentsBinding mCommentsBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initBindings();
    }

    private void initBindings(){
        getSupportActionBar().setTitle("Comments");
        mCommentsBinding = DataBindingUtil.setContentView(this, R.layout.activity_comments);
        CommentActivityViewModel commentViewModel = new CommentActivityViewModel();
        mCommentsBinding.setViewModel(commentViewModel);

        Bundle b = getIntent().getExtras();
        long[] array = b.getLongArray("CommentsId");
        List<CommentItem> comments = new ArrayList<>();
        for(long l : array){
            comments.add(new CommentItem(l));
        }

        CommentsAdapter adapter = new CommentsAdapter(comments);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        mCommentsBinding.comments.setLayoutManager(mLayoutManager);
        mCommentsBinding.comments.setAdapter(adapter);

    }
}
