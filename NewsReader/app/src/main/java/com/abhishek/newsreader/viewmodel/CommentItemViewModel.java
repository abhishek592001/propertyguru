package com.abhishek.newsreader.viewmodel;

import android.databinding.BaseObservable;

import com.abhishek.newsreader.model.CommentItem;
import com.abhishek.newsreader.utils.CommentItemTask;
import com.abhishek.newsreader.utils.Utility;

/**
 * Created by Abhishek on 20-06-2017.
 */

public class CommentItemViewModel extends BaseObservable {

    private CommentItem mComment;

    public CommentItemViewModel(CommentItem comment){
        mComment = comment;
        fetchData();
    }

    public CommentItem getCommentItem(){
        return mComment;
    }

    public String getCommentText(){
        return mComment.Comment;
    }

    public String getChildComment(){
        return mComment.ChildComment;
    }

    public String getBy(){
        return "by " + mComment.By;
    }

    public String getChildBy(){
        return (mComment.ChildBy != null && !mComment.ChildBy.isEmpty() ? "by " + mComment.ChildBy : null);
    }

    public void setComment(CommentItem comment){
        if(mComment.Id != comment.Id){
            mComment = comment;
            if(mComment.Comment.equals(Utility.LOADING_TEXT)){
                fetchData();
            }
            notifyChange();
        }
    }

    private void fetchData(){
        CommentItemTask task = new CommentItemTask(this);
        task.execute();
    }
}
