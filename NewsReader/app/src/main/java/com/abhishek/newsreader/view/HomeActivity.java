package com.abhishek.newsreader.view;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.abhishek.newsreader.R;
import com.abhishek.newsreader.databinding.ActivityHomeBinding;
import com.abhishek.newsreader.model.StoryItem;
import com.abhishek.newsreader.viewmodel.HomeViewModel;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class HomeActivity extends AppCompatActivity implements Observer {

    private ActivityHomeBinding mHomeBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initBindings();
    }

    private void initBindings(){
        mHomeBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        HomeViewModel homeViewModel = new HomeViewModel();
        homeViewModel.addObserver(this);
        homeViewModel.getTopStories();

        mHomeBinding.setViewModel(homeViewModel);

        TopStoriesAdapter adapter = new TopStoriesAdapter(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        mHomeBinding.topStories.setLayoutManager(mLayoutManager);
        mHomeBinding.topStories.setAdapter(adapter);

    }

    @Override
    public void update(Observable o, Object arg) {
        if( o instanceof HomeViewModel && arg != null){
            if(mHomeBinding != null) {
                TopStoriesAdapter adapter = (TopStoriesAdapter) mHomeBinding.topStories.getAdapter();
                adapter.updateStories((List<StoryItem>) arg);
            }
        }
    }
}
