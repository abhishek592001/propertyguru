package com.abhishek.newsreader.utils;

import android.os.AsyncTask;

import com.abhishek.newsreader.model.StoryItem;
import com.abhishek.newsreader.viewmodel.StoryItemViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Abhishek on 20-06-2017.
 */

public class StoryItemTask extends AsyncTask<Void,Void, StoryItem> {

    private StoryItemViewModel mStoryItemViewModel;

    //Unable to mock Async task thas's why test case is not written
    public StoryItemTask(StoryItemViewModel item){
        mStoryItemViewModel = item;
    }

    @Override
    protected StoryItem doInBackground(Void... params) {
        StoryItem item = mStoryItemViewModel.getStory();

        try {
            String response = Utility.getResponse(Utility.ITEM_URL_PREFIX + item.Id + Utility.ITEM_URL_SUFFIX);
            JSONObject json = new JSONObject(response);
            item.Title = json.getString("title");
            item.By = json.getString("by");

            JSONArray array = json.getJSONArray("kids");
            int l = array.length();
            for(int i = 0; i < l; i++){
                item.Comments.add(array.getLong(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return item;
    }

    @Override
    protected void onPostExecute(StoryItem storyItem) {
        super.onPostExecute(storyItem);
        mStoryItemViewModel.notifyChange();
    }
}
