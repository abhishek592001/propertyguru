package com.abhishek.newsreader.view;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.abhishek.newsreader.R;
import com.abhishek.newsreader.databinding.CommentItemRowBinding;
import com.abhishek.newsreader.model.CommentItem;
import com.abhishek.newsreader.model.StoryItem;
import com.abhishek.newsreader.viewmodel.CommentItemViewModel;
import com.abhishek.newsreader.viewmodel.StoryItemViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abhishek on 20-06-2017.
 */

public class CommentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<CommentItem> mComments;

    public CommentsAdapter(List<CommentItem> comments){
        mComments = new ArrayList<>();
        mComments.addAll(comments);
    }

    public void updateComments(List<CommentItem> comments){
        mComments.addAll(comments);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CommentItemRowBinding itemRowBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.comment_item_row, parent, false);
        return new CommentItemViewHolder(itemRowBinding);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((CommentItemViewHolder)holder).bind(mComments.get(position));
    }

    @Override
    public int getItemCount() {
        return mComments.size();
    }

    public class CommentItemViewHolder extends RecyclerView.ViewHolder{
        private CommentItemRowBinding mItemBinding;

        public CommentItemViewHolder(CommentItemRowBinding itemBinding){
            super(itemBinding.commentItem);
            mItemBinding = itemBinding;
        }

        public void bind(CommentItem commentItem){

            if(mItemBinding.getViewModel() == null){
                mItemBinding.setViewModel(new CommentItemViewModel(commentItem));
            }
            else{
                mItemBinding.getViewModel().setComment(commentItem);
            }
        }

    }
}
