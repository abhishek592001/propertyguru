package com.abhishek.newsreader.viewmodel;

import android.databinding.BaseObservable;

import com.abhishek.newsreader.model.StoryItem;
import com.abhishek.newsreader.utils.StoryItemTask;
import com.abhishek.newsreader.utils.Utility;

/**
 * Created by Abhishek on 20-06-2017.
 */

public class StoryItemViewModel extends BaseObservable{

    private StoryItem mStory;

    public StoryItemViewModel(StoryItem story){
        mStory = story;
        fetchStoryData();
    }

    public StoryItem getStory(){
        return mStory;
    }

    public String getTitle(){
        return mStory.Title;
    }

    public void setStory(StoryItem story){
        if(mStory.Id != story.Id){
            mStory = story;

            if(mStory.Title.equals(Utility.LOADING_TEXT)){
                fetchStoryData();
            }
            notifyChange();
        }
    }

    private void fetchStoryData(){
        StoryItemTask storyItemTask = new StoryItemTask(this);
        storyItemTask.execute();
    }
}
