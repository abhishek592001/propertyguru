package com.abhishek.newsreader.viewmodel;

import com.abhishek.newsreader.model.StoryItem;
import com.abhishek.newsreader.utils.TopStoriesTask;
import com.abhishek.newsreader.utils.Utility;

import java.util.List;
import java.util.Observable;

/**
 * Created by Abhishek on 20-06-2017.
 */

public class HomeViewModel extends Observable {

    public HomeViewModel(){

    }

    public void getTopStories(){

        //Unable to mock Async task thas's why test case is not written
        TopStoriesTask topStoriesTask = new TopStoriesTask(this);
        topStoriesTask.execute("topstories");
    }

    public void updateStories(List<StoryItem> stories){
        setChanged();
        notifyObservers(stories);
    }
}
