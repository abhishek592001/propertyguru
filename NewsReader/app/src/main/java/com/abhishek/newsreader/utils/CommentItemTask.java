package com.abhishek.newsreader.utils;

import android.os.AsyncTask;

import com.abhishek.newsreader.model.CommentItem;
import com.abhishek.newsreader.viewmodel.CommentItemViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Abhishek on 20-06-2017.
 */


public class CommentItemTask extends AsyncTask<Void,Void, CommentItem> {

    private CommentItemViewModel mCommentViewModel;

    //Unable to mock Async task thas's why test case is not written
    public CommentItemTask(CommentItemViewModel commentViewModel){
        mCommentViewModel = commentViewModel;
    }

    @Override
    protected CommentItem doInBackground(Void... params) {
        CommentItem item = mCommentViewModel.getCommentItem();
        try {
            String response = Utility.getResponse(Utility.ITEM_URL_PREFIX + item.Id + Utility.ITEM_URL_SUFFIX);
            JSONObject json = new JSONObject(response);
            item.Comment = json.getString("text");
            item.By = json.getString("by");

            JSONArray array = json.getJSONArray("kids");
            if(array.length() > 0){
                item.ChildId = array.getLong(0);
                String childResponse = Utility.getResponse(Utility.ITEM_URL_PREFIX + item.ChildId + Utility.ITEM_URL_SUFFIX);
                JSONObject childJson = new JSONObject(childResponse);
                item.ChildComment = childJson.getString("text");
                item.ChildBy = childJson.getString("by");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return item;
    }

    @Override
    protected void onPostExecute(CommentItem commentItem) {
        super.onPostExecute(commentItem);
        mCommentViewModel.notifyChange();
    }
}
